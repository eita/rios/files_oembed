<?php
/**
 * @copyright Copyright (c) 2017, ownCloud, Inc.
 *
 * @author Andre Luis S Monteiro <andrelsm@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */

namespace OCA\Files_Sharing_oEmbed\Controller;

use OC\Files\Node\Folder;
use OC_Files;
use OC_Util;
use OCA\FederatedFileSharing\FederatedShareProvider;
use OCP\Defaults;
use OCP\IL10N;
use OCP\Template;
use OCP\Share;
use OCP\AppFramework\Controller;
use OCP\IRequest;
use OCP\IURLGenerator;
use OCP\IConfig;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\ISession;
use OCP\IPreview;
use OCA\Files_Sharing\Activity\Providers\Downloads;
use OCP\Files\IRootFolder;
use OCP\Share\Exceptions\ShareNotFound;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use OCP\AppFramework\Http\JSONResponse;

/**
 * Class OembedController
 *
 * @package OCA\Files_Sharing_oEmbed\Controllers
 */
class OembedController extends Controller {
	/** @var IConfig */
        protected $config;

	/** @var IURLGenerator */
        protected $urlGenerator;

	/** @var IUserManager */
        protected $userManager;

	/** @var \OCP\Share\IManager */
        protected $shareManager;

	/** @var IPreview $previewManager */
        protected $previewManager;

	/**
         * @param string $appName
         * @param IRequest $request
         * @param IConfig $config
         * @param IURLGenerator $urlGenerator
         * @param IUserManager $userManager
         * @param ILogger $logger
         * @param \OCP\Activity\IManager $activityManager
         * @param \OCP\Share\IManager $shareManager
         * @param ISession $session
         * @param IPreview $previewManager
         * @param IRootFolder $rootFolder
         * @param FederatedShareProvider $federatedShareProvider
         * @param EventDispatcherInterface $eventDispatcher
         * @param IL10N $l10n
         * @param \OC_Defaults $defaults
         */
        public function __construct($appName,
		IRequest $request,
		IConfig $config,
		IURLGenerator $urlGenerator,
		IUserManager $userManager,
		ILogger $logger,
		\OCP\Activity\IManager $activityManager,
		\OCP\Share\IManager $shareManager,
		ISession $session,
		IPreview $previewManager,
		IRootFolder $rootFolder,
		FederatedShareProvider $federatedShareProvider,
		EventDispatcherInterface $eventDispatcher,
		IL10N $l10n,
		\OC_Defaults $defaults) {

                parent::__construct($appName, $request);

                $this->config = $config;
                $this->urlGenerator = $urlGenerator;
                $this->userManager = $userManager;
                $this->previewManager = $previewManager;
                $this->shareManager = $shareManager;
        }

	/**
	 * CAUTION: the @Stuff turns off security checks; for this page no admin is
	 *          required and no CSRF check. If you don't know what CSRF is, read
	 *          it up in the docs or you might create a security hole. This is
	 *          basically the only required method to add this exemption, don't
	 *          add it to any other method if you don't exactly know what it does
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 */
	public function dump() {
		\OC_User::setIncognitoMode(true);
		$max_width = $this->config->getSystemValue('preview_max_x', 1024);
		$max_height = $this->config->getSystemValue('preview_max_y', 1024);

		// Get share token, width and height
		$token = basename($_GET['url']);
		if ($_GET['maxwidth'])
			$max_width = min($_GET['maxwidth'], $max_width);
		if ($_GET['maxheight'])
			$max_height = min($_GET['maxheight'], $max_height);
		$width = min(320, $max_width);
		$height = min(320, $max_height);

		// Check whether share exists
		try {
			$share = $this->shareManager->getShareByToken($token);
		} catch (ShareNotFound $e) {
			return new JSONResponse(array(), Http::STATUS_NOT_FOUND);
		}

		// Validate share
		if (!$share->getNode()->isReadable() || !$share->getNode()->isShareable())
			return new JSONResponse(array(), Http::STATUS_NOT_FOUND);

		// Build basic digest
		$digest = array(
			'version' => '1.0',
			'title' => $share->getNode()->getName(),
			'author_name' => $this->userManager->get($share->getShareOwner())->getDisplayName(),
		);

		// Build preview
		// TODO: use templates for audio, video and rich content
		switch (dirname($share->getNode()->getMimetype())) {
			case 'audio':
				$digest['type'] = 'video';
				$digest['html'] = '<div class="oembed-audio"><audio tabindex="0" controls="" preload="none" style="max-width: '. $max_width .'px; max-height: '. $max_height .'px"> <source src="'. $this->urlGenerator->linkToRouteAbsolute('files_sharing.sharecontroller.downloadShare', ['token' => $token]) .'" type="'. $share->getNode()->getMimetype() .'"/></audio></div>';
				break;
		
			case 'video':
				$digest['type'] = 'video';
				$digest['html'] = '<div class="oembed-video"><video tabindex="0" controls="" preload="none" style="max-width: '. $max_width .'px; max-height: '. $max_height .'px"> <source src="'. $this->urlGenerator->linkToRouteAbsolute('files_sharing.sharecontroller.downloadShare', ['token' => $token]) .'" type="'. $share->getNode()->getMimetype() .'"/></video></div>';
				break;
		
			case 'image':
				$digest['type'] = 'photo'; 
				$digest['width'] = $width; 
				$digest['height'] = -1; 
				$digest['url'] = $this->urlGenerator->linkToRouteAbsolute('files_sharing.PublicPreview.getPreview', ['x' => $width, 'y' => $height, 'a' => 1, 'file' => $share->getTarget(), 't' => $token]);
				break;
		
			default:
				$digest['type'] = 'rich'; 
				$digest['html'] = '<div class="oembed-rich"><a href="' . $this->urlGenerator->linkToRouteAbsolute('files_sharing.sharecontroller.downloadShare', ['token' => $token]) . '" target="_blank"><img src="' . $this->urlGenerator->getAbsoluteURL(\OC::$server->getMimeTypeDetector()->mimeTypeIcon($share->getNode()->getMimetype())). '"/> <span class="oembed-title">' . $digest['title'] . '</span></a></div>'; 
		}

		return new JSONResponse($digest);
	}

}
