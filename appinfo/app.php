<?php
/**
 * @copyright Copyright (c) 2017, ownCloud, Inc.
 *
 * @author Andre Luis S Monteiro <andrelsm@eita.org.br>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */

use OCP\AppFramework\App;

$app = new App('files_oembed');

$eventDispatcher = \OC::$server->getEventDispatcher();
$eventDispatcher->addListener(
	'OCA\Files_Sharing::loadAdditionalScripts',
	function() {
		$token = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

		\OCP\Util::addHeader('link', [
			'rel' => 'alternate',
			'type' => 'application/json+oembed',
			'href' => \OCP\Util::linkToAbsolute('index.php/apps/files_oembed' , '' , 
					array( 'url' => \OCP\Util::linkToAbsolute($_SERVER['REQUEST_URI']), '' ,'format' => 'json'))
		]);
	}
);

